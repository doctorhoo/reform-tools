#!/bin/sh

set -e

case "$(cat /proc/device-tree/model)" in
	"MNT Reform 2"|"MNT Reform 2 HDMI") : ;;
	"MNT Reform 2 with BPI-CM4 Module") echo "E: refusing to modify eMMC on A311D" >&2; exit 1;;
	"MNT Reform 2 with LS1028A Module") : ;;
	*) echo "E: unknown platform: $(cat /proc/device-tree/model)" >&2; exit 1;;
esac

# only run "reform-flash-uboot emmc" on imx8mq
case "$(cat /proc/device-tree/model)" in "MNT Reform 2"|"MNT Reform 2 HDMI")
	if [ ! -e /sys/class/block/mmcblk0boot0/force_ro ]; then
		echo "/sys/class/block/mmcblk0boot0/force_ro doesn't exist" >&2
		exit 1
	fi

	if [ ! -w /sys/class/block/mmcblk0boot0/force_ro ]; then
		echo "/sys/class/block/mmcblk0boot0/force_ro is not writable" >&2
		exit 1
	fi

	if [ ! -e /boot/flash.bin ]; then
		echo "/boot/flash.bin doesn't exist" >&2
		exit 1
	fi

	for p in $(lsblk --list --noheadings --output=NAME /dev/mmcblk0) mmcblk0boot0 mmcblk0boot1; do
		[ -b "/dev/$p" ] || continue
		if [ -n "$(lsblk --noheadings --output=MOUNTPOINT "/dev/$p")" ]; then
			echo "/dev/$p is still in use" >&2
			exit 1
		fi
	done

	echo "WARNING: This overwrites the bootloader on the eMMC rescue disk with the" >&2
	echo "backup stored as /boot/flash.bin!" >&2

	printf "Are you sure you want to proceed? [y/N] "
	read -r response

	if [ "$response" != "y" ]; then
		echo "Exiting."
		exit
	fi

	reform-flash-uboot --offline emmc

	echo "Bootloader was successfully written to /dev/mmcblk0boot0." >&2
	echo "" >&2
	;;
esac

echo "Do you want to download and install the latest sysimage-v4 to eMMC as well?" >&2
echo "This step needs a working internet connection and either wget or curl installed." >&2
echo "" >&2
echo "WARNING: This overwrites partitions on eMMC, deleting all data." >&2
printf "Are you sure you want to proceed? [y/N] "

read -r response

if [ "$response" != "y" ]; then
	echo "Exiting."
	exit
fi

case "$(cat /proc/device-tree/model)" in
	"MNT Reform 2"|"MNT Reform 2 HDMI") PLATFORM=imx8mq ;;
	"MNT Reform 2 with BPI-CM4 Module") echo "E: refusing to modify eMMC on A311D" >&2; exit 1;;
	"MNT Reform 2 with LS1028A Module") PLATFORM=ls1028a ;;
	*) echo "E: unknown platform: $(cat /proc/device-tree/model)" >&2; exit 1;;
esac

URL="https://source.mnt.re/reform/reform-system-image/-/jobs/artifacts/main/raw/reform-system-${PLATFORM}.img.gz?job=build"
DECOMPRESSOR=gzip

if grep --silent '^URIs: https://reform.debian.net/debian/\?$' /etc/apt/sources.list.d/reform*.sources 2>/dev/null; then
	echo "The current system image is configured to use the Debian stable mirror from" >&2
	echo "reform.debian.net. Do you want to flash the system image from reform.debian.net" >&2
	echo "to your eMMC? If you answer anything but \"y\", the official MNT system image will" >&2
	echo "be downloaded and written to eMMC instead." >&2
	printf "Do you want to download and use the rescue system image from reform.debian.net? [y/N] "

	read -r response
	if [ "$response" = "y" ]; then
		# no need to use the backports kernel because a311d is not
		# allowed to write to emmc anyways
		URL="https://reform.debian.net/images/reform2-system-${PLATFORM}.img.xz"
		DECOMPRESSOR=xz
	fi
fi

echo "Downloading $URL..."

case "$(cat /proc/device-tree/model)" in
	"MNT Reform 2"|"MNT Reform 2 HDMI") MMCDEV="mmcblk0";;
	"MNT Reform 2 with LS1028A Module") MMCDEV="mmcblk1";;
esac

{
if curl --version >/dev/null; then
	curl --silent --location "$URL"
elif wget --version >/dev/null; then
	wget --quiet --output-document=- "$URL"
else
	echo "need curl or wget" >&2
	exit 1
fi; } | "$DECOMPRESSOR" --decompress --to-stdout | dd of="/dev/$MMCDEV" status=progress
partprobe "/dev/$MMCDEV"

# overwrite the partition label
e2label "/dev/${MMCDEV}p1" reformemmcboot
e2label "/dev/${MMCDEV}p2" reformemmcroot

# The rescue system image that we downloaded and wrote to the eMMC above is
# configured to use the rootfs on the SD-Card but we want the initramfs on eMMC
# to boot the rootfs on eMMC and not the rootfs from SD-Card.  Use
# reform-boot-config to update the /etc/fstab and initramfs to use the eMMC
reform-boot-config --emmc emmc

echo "If the SoM dip switch is turned off and no SD-Card is present, your" >&2
echo "system will now boot from eMMC and load the rootfs from there as well." >&2
echo "If you want to boot from eMMC but load the rootfs from elsewhere, run" >&2
echo "reform-boot-config with the --emmc switch." >&2
