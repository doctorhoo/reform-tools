#!/bin/sh
#
# uboot on either emmc or the sd-card (as decided by the position of the DIP
# switch) will load the kernel and initramfs from the first /boot partiton
# where it finds a valid boot.scr (or extlinux.conf). It will attempt to find
# a boot.scr on the first partition of the sd-card first and then will try
# the first partition on emmc.
#
# This script sets up the first partition on the sd-card or emmc (as decided
# by whether the --emmc option was passed or not) such that the initramfs on
# that partition will load the correct rootfs. The rootfs can either reside on
# the sd-card, emmc, nvme, sata or a usb mass storage device.
#
# The initramfs is tightly tied to the rootfs and the kernel version because it
# is generated from the contents of the rootfs and contains the kernel modules
# that must fit the correct kernel version. The choice of rootfs stored inside
# the initramfs is derived from the settings of /etc/fstab in the rootfs.

set -eu

if [ "$(id -u)" -ne 0 ]
  then echo "reform-boot-config has to be run as root / using sudo."
  exit
fi

echo "This script selects your preferred boot medium. It writes your choice to the file /etc/fstab"
echo

case $(cat /proc/device-tree/model) in
	"MNT Reform 2"|"MNT Reform 2 HDMI") MMCDEV="mmcblk0"; SDDEV="mmcblk1"; NVMEDEV=nvme0n1; USBDEV=sda ;;
	"MNT Reform 2 with BPI-CM4 Module")                   SDDEV="mmcblk0"; NVMEDEV=nvme0n1; USBDEV=sda ;;
	"MNT Reform 2 with LS1028A Module") MMCDEV="mmcblk1"; SDDEV="mmcblk0"; NVMEDEV=sda;     USBDEV=sdb ;;
	*) echo "E: unknown platform: $(cat /proc/device-tree/model)" >&2; exit 1;;
esac

usage() {
	echo "Usage: " >&2
	echo "  reform-boot-config [--emmc] sd    # rootfs on SD card (/dev/${SDDEV}p2, default)." >&2
	echo "  reform-boot-config [--emmc] nvme  # rootfs on NVMe SSD (/dev/${NVMEDEV}p1)." >&2
	echo "  reform-boot-config [--emmc] usb   # rootfs on USB storage device (/dev/${USBDEV}1)." >&2
	echo "  reform-boot-config [--emmc] emmc  # rootfs on eMMC (/dev/${MMCDEV}p2)." >&2
	echo "" >&2
	echo "      --emmc Record boot preference on eMMC instead of SD card." >&2
	echo "             This is only useful with SoM dip switch turned off." >&2
	echo "" >&2
	echo "Choosing sd, nvme, usb or emmc will set the root partition to" >&2
	echo "/dev/${SDDEV}p2, /dev/${NVMEDEV}p1, /dev/${USBDEV}1 or /dev/${MMCDEV}p2," >&2
	echo "respectively. You can choose another root partition by passing" >&2
	echo "the absolute device path starting with /dev/ explicitly." >&2
	echo "For example, to boot a rootfs on an LVM volume, run:" >&2
	echo "" >&2
	echo "    reform-boot-config /dev/reformvg/root" >&2
}

if [ $# -ne 1 ] && [ $# -ne 2 ]; then
	usage
	exit 1
fi

BOOTPREF="$1"
BOOTPART="${SDDEV}p1"

if [ "--emmc" = "$1" ]; then
	case "$(cat /proc/device-tree/model)" in "MNT Reform 2 with BPI-CM4 Module")
		echo "E: writing to eMMC not supported on A311D" >&2
		exit 1
		;;
	esac

	BOOTPREF="$2"
	BOOTPART="${MMCDEV}p1"

	if [ ! -b "/dev/$BOOTPART" ]; then
		echo "/dev/$BOOTPART doesn't exist -- run reform-flash-rescue" >&2
		exit 1
	fi
fi

case $BOOTPREF in
	sd)   : "${ROOTPART:=${SDDEV}p2}";;
	nvme) : "${ROOTPART:=${NVMEDEV}p1}";;
	usb)  : "${ROOTPART:=${USBDEV}1}";;
	emmc) : "${ROOTPART:=${MMCDEV}p2}";;
	/dev/*)
		if [ ! -b "$BOOTPREF" ]; then
			echo "there is no block device called $BOOTPREF" >&2
			exit 1
		fi
		: "${ROOTPART:=$BOOTPREF}"
		;;
	*) usage; exit 1;;
esac

ROOTPART="${ROOTPART#/dev/}"

# POSIX shell only has a single array: $@
# Instead of storing the list of directories that need to be unmounted or
# removed in a string separated by whitespaces (and thus not supporting
# whitespace characters in path names) we use $@ to store that list.
# We push each new entry to the beginning of the list so that we can use
# shift to pop the first entry.
cleanup() {
	: "${TMPDIR:=/tmp}"
	for dir; do
		# $dir is either a device that has to be unmounted or an empty
		# temporary directory that used to be a mountpoint
		echo "cleaning up $dir" >&2
		ret=0
		if [ -d "$dir" ]; then
			# special handling for /dev, /sys and /proc
			case "$dir" in
				*/dev|*/sys|*/proc) umount "$dir" || ret=$?;;
				*) rmdir "$dir" || ret=$?;;
			esac
		else
			umount "$dir" || ret=$?
		fi
		if [ "$ret" != 0 ]; then
			echo "cleaning up $dir failed" >&2
		fi
		# remove this item from $@
		shift
	done
	if [ "${MOUNTROOT:-}" = "/" ]; then
		ret=0
		mount /boot || ret=$?
		if [ "$ret" != 0 ]; then
			echo "mounting /boot failed" >&2
		fi
	fi
	echo reform-boot-config FAILED to run >&2
}
set --
trap 'cleanup "$@"' EXIT INT TERM

# check if rootfs is already mounted somewhere that is not /
MOUNTROOT="$(lsblk --noheadings --output=MOUNTPOINT "/dev/$ROOTPART")"
if [ "$MOUNTROOT" != "" ] && [ "$MOUNTROOT" != "/" ]; then
	echo "/dev/$ROOTPART is still mounted on $MOUNTROOT." >&2
	echo "Please unmount before running this script" >&2
	exit 1
fi

# mount the desired root partition somewhere if it isn't mounted yet
if [ "$MOUNTROOT" = "" ]; then
	MOUNTROOT="$(mktemp --tmpdir --directory reform-boot-config.XXXXXXXXXX)"
	set -- "/dev/$ROOTPART" "$MOUNTROOT" "$@"
	mount "/dev/$ROOTPART" "$MOUNTROOT"
fi

if [ ! -d "$MOUNTROOT/boot" ]; then
	echo "the rootfs does not contain a /boot directory" >&2
	exit 1
fi

# find the device that was mounted as /boot according to the /etc/fstab in the
# given rootfs
OLDBOOTPART="$(LIBMOUNT_FSTAB="$MOUNTROOT/etc/fstab" findmnt --fstab --noheadings --evaluate --mountpoint /boot --output SOURCE)"
if [ -z "$OLDBOOTPART" ]; then
	echo "cannot find /boot device referenced by /etc/fstab in rootfs at /dev/$ROOTPART" >&2
	exit 1
fi
if [ ! -e "$OLDBOOTPART" ]; then
	echo "/boot device $OLDBOOTPART from /etc/fstab in /dev/$ROOTPART doesn't exist" >&2
	exit 1
fi

# check if the new boot is still mounted somewhere
if [ -n "$(lsblk --noheadings --output=MOUNTPOINT "/dev/$BOOTPART")" ]; then
	echo "/dev/$BOOTPART is still mounted somewhere, which means that it is" >&2
	echo "probably used by the currently running system and that replacing" >&2
	echo "its contents might make the currently running system unbootable."
	# if the new boot partition is found to be mounted but
	# it's not currently mounted as /boot
	# or it's not on the sd-card
	# or it doesn't have the reformsdboot label
	# then immediately exit
	if [ "$(lsblk --noheadings --output=MOUNTPOINT "/dev/$BOOTPART")" != "/boot" ] \
		|| [ "$BOOTPART" != "${SDDEV}p1" ] \
		|| [ "$(lsblk --noheadings --output=LABEL "/dev/$BOOTPART")" != "reformsdboot" ]; then
		echo "Please unmount before running this script" >&2
		exit 1
	fi
	# otherwise, this is very likely a rescue system sd-card so we suggest
	# to automatically umount it and overwrite its contents
	echo "It seems that the current /boot partition is only booting a rescue" >&2
	echo "system on SD-Card.">&2
	echo "" >&2
	printf "Do you want to unmount it? [y/N]"
	read -r response
	if [ "$response" != "y" ]; then
		echo "Exiting."
		exit
	fi
	umount "/dev/$BOOTPART"
fi

# check that the new mountpoint for /boot is empty
if mountpoint --quiet "$MOUNTROOT/boot"; then
	echo "Something is still mounted on $MOUNTROOT/boot." >&2
	echo "Please unmount before running this script" >&2
	exit 1
fi

# mount the new boot partition
set -- "/dev/$BOOTPART" "$@"
mount "/dev/$BOOTPART" "$MOUNTROOT/boot"

if [ "$OLDBOOTPART" = "/dev/$BOOTPART" ]; then
	echo "the /boot partition /dev/$BOOTPART referenced by the rootfs at /dev/$ROOTPART remains the same" >&2
else
	# copy the contents of the old /boot to the new /boot
	OLDMOUNTBOOT="$(lsblk --noheadings --output=MOUNTPOINT "$OLDBOOTPART")"
	needumount="no"
	if [ "$OLDMOUNTBOOT" = "" ]; then
		OLDMOUNTBOOT="$(mktemp --tmpdir --directory reform-boot-config.XXXXXXXXXX)"
		set -- "$OLDBOOTPART" "$OLDMOUNTBOOT" "$@"
		mount "$OLDBOOTPART" "$OLDMOUNTBOOT"
		needumount="yes"
	fi

	rsync -axHAWXS --numeric-ids --quiet "$OLDMOUNTBOOT/" "$MOUNTROOT/boot"
	if [ "$needumount" = "yes" ]; then
		[ "$1" = "$OLDBOOTPART" ]  && shift && umount "$OLDBOOTPART"
		[ "$1" = "$OLDMOUNTBOOT" ] && shift && rmdir "$OLDMOUNTBOOT"
	fi
fi

if LIBMOUNT_FSTAB="$MOUNTROOT/etc/fstab" findmnt --fstab --noheadings --source "/dev/$ROOTPART" --mountpoint "/" >/dev/null && \
	LIBMOUNT_FSTAB="$MOUNTROOT/etc/fstab" findmnt --fstab --noheadings --source "/dev/$BOOTPART" --mountpoint "/boot" >/dev/null; then
	echo "/etc/fstab already contains the correct entries" >&2
else
	echo "commenting original /etc/fstab contents" >&2

	SWAP=
	if LIBMOUNT_FSTAB="$MOUNTROOT/etc/fstab" findmnt --fstab --types swap >/dev/null; then
		SWAP="$(LIBMOUNT_FSTAB="$MOUNTROOT/etc/fstab" findmnt --noheadings --fstab --types swap --output SOURCE,TARGET,FSTYPE,OPTIONS,FREQ,PASSNO)"
	fi

	sed -i -e 's/^/#/' "$MOUNTROOT/etc/fstab"
	cat << END >> "$MOUNTROOT/etc/fstab"
/dev/$ROOTPART / auto errors=remount-ro 0 1
/dev/$BOOTPART /boot auto errors=remount-ro 0 1
END

	if [ -n "$SWAP" ]; then
		echo "$SWAP" >> "$MOUNTROOT/etc/fstab"
	fi
fi

if [ "$MOUNTROOT" = "/" ]; then
	update-initramfs -u
else
	set -- "$MOUNTROOT/proc" "$MOUNTROOT/sys" "$MOUNTROOT/dev" "$@"
	mount -o bind /dev "$MOUNTROOT/dev"
	mount -t sysfs sys "$MOUNTROOT/sys"
	mount -t proc proc "$MOUNTROOT/proc"
	chroot "$MOUNTROOT" update-initramfs -u
	[ "$1" = "$MOUNTROOT/proc" ] && shift && umount "$MOUNTROOT/proc"
	[ "$1" = "$MOUNTROOT/sys" ]  && shift && umount "$MOUNTROOT/sys"
	[ "$1" = "$MOUNTROOT/dev" ]  && shift && umount "$MOUNTROOT/dev"
fi

# unmount /boot partition
[ "$1" = "/dev/$BOOTPART" ] && shift && umount "/dev/$BOOTPART"

# unmount the root partition if necessary
if [ "$MOUNTROOT" != "/" ]; then
	[ "$1" = "/dev/$ROOTPART" ] && shift && umount "/dev/$ROOTPART"
	[ "$1" = "$MOUNTROOT" ]     && shift && rmdir "$MOUNTROOT"
fi

# make sure that the cleanup array is empty now
[ $# -eq 0 ]

trap - EXIT INT TERM

# make sure that the new boot partition does not still have the reformsdboot
# or reformemmcboot labels in case it used to be the /boot partition from a
# rescue system on sd-card or emmc, respectively
case "$BOOTPART" in
	"${SDDEV}p1")
		if [ "$(lsblk --noheadings --output=LABEL "/dev/$BOOTPART")" = "reformsdboot" ]; then
			e2label "/dev/$BOOTPART" ""
		fi
		;;
	"${MMCDEV}p1")
		if [ "$(lsblk --noheadings --output=LABEL "/dev/$BOOTPART")" = "reformemmcboot" ]; then
			e2label "/dev/$BOOTPART" ""
		fi
		;;
esac

# since /boot had to be unmounted before running this script, make sure to
# mount it again
if [ "$MOUNTROOT" = "/" ]; then
	mount /boot
fi

case "$(cat /proc/device-tree/model)" in
	"MNT Reform 2 with BPI-CM4 Module") echo "Your /boot partition is on your SD-Card (/dev/$BOOTPART)." >&2 ;;
	*)	if [ "$BOOTPART" = "${MMCDEV}p1" ]; then
			echo "Your /boot partition is on emmc (/dev/$BOOTPART)." >&2
		else
			echo "Your /boot partition is on your SD-Card (/dev/$BOOTPART)." >&2
		fi
	;;
esac

echo "Restart MNT Reform (type: reboot) after saving your work to activate the changes."
